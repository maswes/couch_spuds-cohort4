%% receive_signal.m
% Use this file to read in the .mat file from the modem
% Items to update are:
%   - line 13:  update the name of the .mat file for the file being received
%   - line 20:  update the location of the pn312_sym.mat file used in
%               transmit_signal.m. This assumes it is located one level
%               above this script
%   - line 74:  update the name of the desired output .mat file
%

clear; clc;
% load('ts_encoded_signal_1_demod.mat');
load('receive_in_3.mat'); % Change to name of mat file received from the modem
signal = signal_demod;
clear signal_demod

frame_size = 832;
num_data = 78;

load('../pn312_sym.mat'); % loads as pn312 symbols (for un-randomizing)

% signal = signal(1:frame_size*5);

num_blocks = length(signal)/frame_size;

rx_signal = reshape(signal,frame_size,[]);

% Gray coding
%         data_symbols(data_symbols == 0) = -7;
%         data_symbols(data_symbols == 1) = -5;
%         data_symbols(data_symbols == 3) = -3;
%         data_symbols(data_symbols == 2) = -1;
%         data_symbols(data_symbols == 6) = 1;
%         data_symbols(data_symbols == 4) = 3;
%         data_symbols(data_symbols == 5) = 5;
%         data_symbols(data_symbols == 7) = 7;

gray_code = [-7,-5,-1,-3,3,5,1,7]; %index 0-7 is the symbol value

rx_data = [];
symbol_size = 3;
data = [];

for i = 1:num_blocks
    if(mod(i,num_data+1) ~= 1) % if it is not a field sync, take data
        rx_data = rx_signal(:,i);
        
        rx_symbol = [];
        for j = 1:length(rx_data) % for each entry - convert to 0-7
            rx_symbol(j) = find(rx_data(j) == gray_code)-1;
        end
        % rx_symbol is a row vector of 0-7 
        
        % De-randomize the data
        rx_symbol_deran = bitxor(uint8(rx_symbol),pn312_sym','uint8');       
        
        % ADDED TO OUTPUT 0-7 for decoding
%         data = [data; rx_symbol'];
        data = [data; rx_symbol_deran'];
                
        % convert 3 bit symbols to 8 bit bytes
%         bits = dec2bin(rx_symbol_deran, symbol_size);
%         bits2 = [];
%         for j = 1:frame_size
%             bits2 = [bits2, bits(j,:)];
%         end
%         % Convert bits to 8 bit byte
%         for k = 1:8:length(bits2)
%             data = [data; uint8(bin2dec(bits2(k:k+7)))]; 
%         end      
    end
end

save('decode_in_3.mat','data'); % Change to correspond 


