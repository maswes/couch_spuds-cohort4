%% transmit_signal.m
% Use this file to create the .mat file to be transmitted
% This file also generates the 312 byte PN-sequence used to randomize bits
% further.
%
% Items needed to run:
%   - Need pn128.mat file used to define the start of file (SOF)
%   - Need Fsync.mat file used to define the field sync
%
% Items to update are:
%   - line 43:  update the name of the .mat file of the encoded output.
%               This file adds field sync information to the transmit signal.
%   - line 52:  update the location of Fsync.mat used for field sync
%   - line 54:  update the location of the pn128.mat file used to define
%               the SOF
%   - line 102:  update the name of the desired output .mat file
%

% Generate 312 byte pn-sequence
clear; clc;
hpn = comm.PNSequence('Polynomial',[5 3 0],'SamplesPerFrame',312*8, 'InitialConditions',[0 0 0 0 1]);
prbs = step(hpn)';
pn312 = [];
for i = 1:8:length(prbs)
    num = dec2bin(prbs(i:i+7));
    pn312 = [pn312; uint8(bin2dec(num'))];
end

% Convert to symbols
pn312_sym = [];
for i = 1:3:length(prbs)
    n = dec2bin(prbs(i:i+2));
    pn312_sym = [pn312_sym; uint8(bin2dec(n'))];
end

save('pn312.mat','pn312');
save('pn312_sym.mat','pn312_sym');


% Read in encoded ts file 
clear; clc;

load('720_encoded_video.mat');
video = encoded_video;
clear encoded_video;

num_bytes = 832;

num_index = length(video)/num_bytes;
num_data = 78;

q = open('../Fsync.mat');
field = q.Field_Sync';
load('../pn128.mat'); % loads as pn128
% seg_sync = field(1:4);

load('../pn312_sym.mat'); % loads as pn312 (used to randomize data)

SOF = field;
SOF(705:end) = pn128'; % add a 127-pn sequence followed by 1
signal = SOF; % Add SOF (start of file): Add extra +1/-1 to differentiate the beginning
%
% Pad end of signal
video2 = [video; zeros(ceil(num_index)*num_bytes-length(video),1)];

vid = reshape(video2,num_bytes,[]);

num_bits = 3;

gray_code = [-7,-5,-1,-3,3,5,1,7]; %index 0-7 is the symbol value

[num_bytes,num_frames] = size(vid);

num_frames = floor(num_frames/num_data);

for i = 1:num_frames
    index = (i-1)*num_data + 1;
    for j = index:index+num_data-1
        % Randomize bits
        rand_vid(:,j) = bitxor(vid(:,j),pn312_sym,'uint8');
        %%%% Convert 8-bit bytes to 3-bit symbols 
        bits = dec2bin(rand_vid(:,j), num_bits);
        
        bits2 = [];
        for m = 1:num_bytes
            bits2 = [bits2, bits(m,:)];
        end
        data_symbols = [];
        for k = 1:3:length(bits2)
            data_symbols = [data_symbols, gray_code(bin2dec(bits2(k:k+2))+1)]; 
        end

        signal = [signal, data_symbols];
    end
    % Add field sync
    if(i ~= num_frames)
        signal = [signal, field];
    end
end
signal = signal';

save('ts_encoded_signal_1.mat','signal')