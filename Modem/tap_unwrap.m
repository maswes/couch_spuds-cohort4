function [y] = tap_unwrap(x)
% receive a vector of tap history, remove all zeros and unwrap

%copy without zeros
for nn=1:length(x)
    if x(nn) == 0
        break;
    end
    y(nn) = x(nn);
end

y = fliplr(y);

wrap = 0;

for nn=1:length(y)
    if wrap
        y(nn) = y(nn) + 8;
    elseif y(nn) == 8
        wrap = 1;
    end
end

end