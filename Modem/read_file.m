% script top convert data samples format from GNU Radio to MATLAB

fileID_r = fopen('bach.txt','r');
bach = fread(fileID_r,'float');
fclose(fileID_r);
dtv_data_rx = complex(bach(1:2:end), bach(2:2:end));